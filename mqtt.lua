-- Author: Norberto Hideaki Enomoto
-- Date: 10/24/2016

-- configure ESP as a station
-- wifi.setmode(wifi.STATION)
-- wifi.sta.config("","")
-- wifi.sta.autoconnect(1)
-- print("[Connected, IP is "..wifi.sta.getip().."]");

led1 = 0
led2 = 1
gpio.mode(led1, gpio.OUTPUT)
gpio.mode(led2, gpio.OUTPUT)

-- Configuration to connect to the MQTT broker.
BROKER = "m11.cloudmqtt.com"   -- Ip/hostname of MQTT broker
BRPORT = 15219                 -- MQTT broker port
BRUSER = ""            -- If MQTT authenitcation is used then define the user
BRPWD  = ""        -- The above user password
CLIENTID = "ESP8266-" ..  node.chipid() -- The MQTT ID. Change to something you like

-- MQTT topics to subscribe
topics = {"kibana"} -- Add/remove topics to the array

-- Control variables.
pub_sem = 0         -- MQTT Publish semaphore. Stops the publishing whne the previous hasn't ended
current_topic  = 1  -- variable for one currently being subscribed to
topicsub_delay = 50 -- microseconds between subscription attempts, worked for me (local network) down to 5...YMMV
id1 = 0
id2 = 0

-- connect to the broker
print "Connecting to MQTT broker. Please wait..."
m = mqtt.Client( CLIENTID, 120, BRUSER, BRPWD)
m:connect( BROKER , BRPORT, 0, function(conn)
     print("Connected to MQTT:" .. BROKER .. ":" .. BRPORT .." as " .. CLIENTID )
     mqtt_sub() --run the subscription function
end)

function mqtt_sub()
     if table.getn(topics) < current_topic then
          -- if we have subscribed to all topics in the array, run the main prog
          run_main_prog()
     else
          --subscribe to the topic
          m:subscribe(topics[current_topic] , 0, function(conn)
               print("Subscribing topic: " .. topics[current_topic - 1] )
               gpio.write(led1, gpio.LOW)
          end)
          current_topic = current_topic + 1  -- Goto next topic
          --set the timer to rerun the loop as long there is topics to subscribe
          tmr.alarm(5, topicsub_delay, 0, mqtt_sub )
     end
end

-- Sample publish functions:
function publish_ligada()
     m:publish("kibana","Enabled",0,0, function(conn) 
        print("Sending data: Ligada")
     end)  
end

function publish(data)
     m:publish("kitri",data,0,0, function(conn) 
        print("Sending data: " .. data)
     end)   
end

--main program to run after the subscriptions are done
function run_main_prog()
     print("Main program")
     
     -- tmr.alarm(2, 5000, 1, publish_data1 )
     -- tmr.alarm(3, 6000, 1, publish_data2 )
               
     -- Callback to receive the subscribed topic messages. 
     m:on("message", function(conn, topic, data)        
        if (data ~= nil ) then
          if (data == "0") then
             gpio.write(led1, gpio.HIGH);
             publish("ESP8266: Disabled")
          end
          if (data == "1") then
             gpio.write(led1, gpio.LOW);
             publish("ESP8266: Enabled")
          end
          if (data == "2") then
             if (gpio.read(led1) == gpio.HIGH) then
                publish("check status ESP8266: Disabled")
             end
             if (gpio.read(led1) == gpio.LOW) then
                publish("check status ESP8266: Enabled")
             end
          end
          print("Receiving data: " .. data)          
        end
      end )
end
